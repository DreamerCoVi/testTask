/* eslint-disable @typescript-eslint/no-var-requires */
const path = require('path');

const HtmlWebpackPlugin = require('html-webpack-plugin');
const ReactRefreshWebpackPlugin = require('@pmmmwh/react-refresh-webpack-plugin');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const EsLintPlugin = require('eslint-webpack-plugin');

const isDevelopment = process.env.NODE_ENV === 'development';
const mode = isDevelopment ? 'development' : 'production';
const target = isDevelopment ? 'web' : 'browserslist';

const plugins = [
  new HtmlWebpackPlugin({
    template: path.resolve(__dirname, 'public', 'index.html'),
    favicon: './src/favicon.ico',
    minify: {
      collapseWhitespace: !isDevelopment,
    },
  }),
  new ForkTsCheckerWebpackPlugin({
    async: false,
  }),
  new EsLintPlugin({
    extensions: ['ts', 'tsx'],
  }),
  isDevelopment && new ReactRefreshWebpackPlugin(),
].filter(Boolean);

const optimization = () => {
  const config = {};

  if (!isDevelopment) {
    config.minimizer = [new TerserPlugin()];
  }

  return config;
};

module.exports = {
  mode,
  plugins,
  target,
  entry: path.resolve(__dirname, 'src', 'index.tsx'),
  output: {
    publicPath: '/',
    clean: true,
    path: path.resolve(__dirname, 'dist'),
    assetModuleFilename: 'assets/[hash][ext][query]',
    filename: '[name].[contenthash].bundle.js',
  },
  resolve: {
    extensions: ['.ts', '.tsx', '.js'],
    modules: ['./src', 'node_modules'],
  },

  performance: {
    maxEntrypointSize: 512000,
    maxAssetSize: 512000,
  },

  optimization: {
    minimize: true,
    ...optimization(),
  },

  devServer: {
    port: 3000,
    historyApiFallback: true,
    liveReload: true,
    compress: true,
    bonjour: true,
    hot: 'only',
    static: {
      directory: path.resolve(__dirname, 'dist'),
      watch: true,
    },
  },

  module: {
    rules: [
      {
        test: /\.(jsx|js|tsx|ts)$/,
        include: path.resolve(__dirname, 'src'),
        exclude: /node_modules/,
        use: 'ts-loader',
      },
      {
        test: /\.(s[ac]|c)ss$/i,
        use: ['style-loader', 'css-loader', 'postcss-loader', 'sass-loader'],
      },
      {
        test: /\.(png|jpe?g|gif|svg|webp|ico)$/i,
        type: isDevelopment ? 'asset/resource' : 'asset',
      },
      {
        test: /\.(woff2?|eot|ttf|otf)$/i,
        type: 'asset/resource',
      },
    ],
  },
};
