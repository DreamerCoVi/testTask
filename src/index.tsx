import React from 'react';
import ReactDOM from 'react-dom/client';
import { BrowserRouter } from 'react-router-dom';
import App from './App';
import 'css/main.css';
import { ThemeProvider } from '@mui/material/styles';
import { theme } from 'theme';

const root = ReactDOM.createRoot(document.getElementById('react') as Element);

root.render(
  <BrowserRouter>
    <ThemeProvider theme={theme}>
      <App />
    </ThemeProvider>
  </BrowserRouter>,
);
