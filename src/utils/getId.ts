import { SinglePeople } from 'types/people';
export const getId = (url: SinglePeople['url']) => url.split('/').at(-2);
