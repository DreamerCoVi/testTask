import { toast } from 'react-toastify';

export const getFromLocalStorage = (key: string) => {
  const content = localStorage.getItem(key);
  if (content) {
    return JSON.parse(content);
  }
  return null;
};

export const setToLocalStorage = (key: string, value: any) => {
  /* save request */
  const serializedValue = JSON.stringify(value);
  localStorage.setItem(key, serializedValue);
  toast.success('Saved');
};

export const removeFromLocalStorge = (key: string) => {
  localStorage.removeItem(key);
  return 1;
};
