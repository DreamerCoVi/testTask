import CardPage from 'pages/CardPage';
import ListPage from 'pages/ListPage';
import React from 'react';
import { Navigate, Route, Routes } from 'react-router';

const App = () => {
  return (
    <div>
      <Routes>
        <Route path="/">
          <Route index element={<Navigate to="/cards" />} />
          <Route path="cards">
            <Route index element={<ListPage />} />
            <Route path=":cardId" element={<CardPage />} />
          </Route>
        </Route>
      </Routes>
    </div>
  );
};

export default App;
