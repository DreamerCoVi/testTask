import { Container } from '@mui/system';
import { observer } from 'mobx-react';
import React, { useEffect } from 'react';
import { useParams } from 'react-router';
import { cardStore } from 'store/card';
import { MidscreenLoader } from 'ui';
import FullCard from './modules/FullCard';

const CardPage = observer(() => {
  const { cardId } = useParams();

  useEffect(() => {
    if (!cardStore.cardData?.name) {
      cardStore.getPeopleCard(cardId as string);
    }

    return () => cardStore.setCardData(null);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  if (!cardStore.cardData?.name || cardStore.isLoading) {
    return <MidscreenLoader />;
  }

  return (
    <Container maxWidth="lg" style={{ display: 'flex', justifyContent: 'center' }}>
      <FullCard />
    </Container>
  );
});

export default CardPage;
