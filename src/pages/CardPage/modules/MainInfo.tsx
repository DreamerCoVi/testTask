import { Divider, Stack, Typography } from '@mui/material';
import { Box } from '@mui/system';
import React from 'react';
import { cardStore } from 'store/card';
import CardField from './CardField';

const MainInfo = () => {
  const { cardData } = cardStore;
  return (
    <Box>
      <Typography sx={{ mb: 2 }} gutterBottom align="center" variant="h5">
        {cardData.name}
      </Typography>
      <Stack
        justifyContent={'center'}
        direction={{
          xs: 'column',
          sm: 'row',
        }}
        divider={<Divider orientation="vertical" flexItem />}
        spacing={2}
      >
        <CardField label={'Height'} fieldName={'height'} type="number" />
        <CardField label={'Gender'} fieldName={'gender'} type="text" />
        <CardField label={'Mass'} fieldName={'mass'} type="number" />
      </Stack>
    </Box>
  );
};

export default MainInfo;
