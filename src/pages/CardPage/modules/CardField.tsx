import { observer } from 'mobx-react-lite';
import React, { FC } from 'react';
import { cardStore } from 'store/card';
import { SinglePeople } from 'types/people';
import { SwitchableTextField, SwitchableTextFieldProps } from 'ui';

interface CardFieldProps extends SwitchableTextFieldProps {
  fieldName: keyof SinglePeople;
}

const CardField: FC<CardFieldProps> = observer((props) => {
  const { cardData, changeCardData } = cardStore;
  const { fieldName, ...inputProps } = props;

  const handleBlur = (e: React.FocusEvent<HTMLInputElement>) => {
    changeCardData(fieldName, e.target.value);
  };

  return (
    <SwitchableTextField
      size="small"
      required
      onBlur={handleBlur}
      defaultValue={cardData[fieldName]}
      key={cardData[fieldName] as string}
      sx={{ width: 100 }}
      {...inputProps}
    />
  );
});

export default CardField;
