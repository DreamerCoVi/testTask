import { Box } from '@mui/system';
import { observer } from 'mobx-react-lite';
import React from 'react';
import { cardStore } from 'store/card';
import { Avatar } from 'ui';

const AvatarContainer = observer(() => {
  const { cardData } = cardStore;
  return (
    <Box
      style={{
        position: 'absolute',
        left: '50%',
        top: 0,
        transform: 'translate(-50%, -50%)',
      }}
    >
      <Avatar query={'200x200'} alt={cardData?.name} />
    </Box>
  );
});

export default AvatarContainer;
