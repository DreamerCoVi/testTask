import { Divider } from '@mui/material';
import { Box, Stack } from '@mui/system';
import React from 'react';
import { CardSubtitle } from 'ui';
import CardField from './CardField';

const Appearance = () => {
  return (
    <Box>
      <CardSubtitle>Appearance</CardSubtitle>

      <Stack
        justifyContent={'center'}
        direction={{
          xs: 'column',
          sm: 'row',
        }}
        spacing={2}
        divider={<Divider orientation="vertical" flexItem />}
      >
        <CardField label={'Hair color'} fieldName={'hair_color'} type="text" />
        <CardField label={'Eye color'} fieldName={'eye_color'} type="text" />
        <CardField label={'Skin color'} fieldName={'skin_color'} type="text" />
      </Stack>
    </Box>
  );
};

export default Appearance;
