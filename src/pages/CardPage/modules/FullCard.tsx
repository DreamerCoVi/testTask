import { Paper } from '@mui/material';
import { observer } from 'mobx-react';
import React from 'react';
import Appearance from './Appearance';
import Links from './Links';
import MainInfo from './MainInfo';
import Header from './Header';
import AvatarContainer from './AvatarContainer';

const FullCard = observer(() => {
  return (
    <Paper
      elevation={5}
      sx={{
        maxWidth: 750,
        width: '100%',
        p: {
          xs: 1,
          sm: 3,
        },
        position: 'relative',
        mt: 17,
        pt: 2,
      }}
    >
      <AvatarContainer />
      <Header />
      <MainInfo />
      <Appearance />
      <Links />
    </Paper>
  );
});

export default FullCard;
