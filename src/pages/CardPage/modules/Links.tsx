import { Box, Divider, Link } from '@mui/material';
import { Stack } from '@mui/system';
import { observer } from 'mobx-react-lite';
import React, { useCallback } from 'react';
import { cardStore } from 'store/card';
import { CardSubtitle, InfoBlock } from 'ui';

const Links = observer(() => {
  const renderList = useCallback(
    (field: 'films' | 'species' | 'vehicles') => (
      <Stack>
        {cardStore.cardData[field].map((item) => (
          <Link variant="body2" gutterBottom underline="hover" key={item} href={item}>
            {item.split('/').slice(-3).join('/')}
          </Link>
        ))}
      </Stack>
    ),
    [],
  );

  return (
    <Box>
      <CardSubtitle>Links</CardSubtitle>
      <Stack
        justifyContent={'center'}
        direction={{
          xs: 'column',
          sm: 'row',
        }}
        spacing={2}
        divider={<Divider orientation="vertical" flexItem />}
      >
        <InfoBlock label="Films">{renderList('films')}</InfoBlock>
        <InfoBlock label="Species">{renderList('species')}</InfoBlock>
        <InfoBlock label="Vehicles">{renderList('vehicles')}</InfoBlock>
      </Stack>
    </Box>
  );
});

export default Links;
