import { Box, Button, Typography } from '@mui/material';
import { Stack } from '@mui/system';
import dayjs from 'dayjs';
import { observer } from 'mobx-react';
import React, { useMemo } from 'react';
import { useNavigate } from 'react-router';
import { cardStore } from 'store/card';

const Header = observer(() => {
  const { cardData, saveCard, resetCard, clearCardData } = cardStore;

  const navigate = useNavigate();

  const created = useMemo(
    () => `Created: ${dayjs(cardData.created).format('DD.MM.YYYY')}`,
    [cardData.created],
  );
  const edited = useMemo(
    () => `Edited: ${dayjs(cardData.edited).format('DD.MM.YYYY')}`,
    [cardData.edited],
  );

  const handleBack = () => {
    setTimeout(() => {
      clearCardData();
    }, 400);
    navigate('/cards');
  };

  return (
    <Box
      sx={{
        mb: 3,
        mt: {
          xs: 11,
          sm: 6,
          md: 1,
        },
      }}
    >
      <Stack direction="row" justifyContent={'space-between'}>
        <Box>
          <Typography style={{ color: '#CCCCCC' }} variant="caption" component="div">
            {created}
          </Typography>
          <Typography style={{ color: '#CCCCCC' }} variant="caption" component="div">
            {edited}
          </Typography>
        </Box>
        <Box>
          <Stack direction="row" spacing={2}>
            <Button onClick={handleBack} variant="contained" color="info" size="small">
              Back
            </Button>
            <Button onClick={resetCard} variant="contained" color="error" size="small">
              Reset
            </Button>
            <Button onClick={saveCard} variant="contained" color="success" size="small">
              Save
            </Button>
          </Stack>
        </Box>
      </Stack>
    </Box>
  );
});

export default Header;
