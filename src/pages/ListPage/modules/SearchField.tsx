import { Paper, TextField } from '@mui/material';
import useDebounce from 'hooks/useDebounce';
import { observer } from 'mobx-react-lite';
import React, { useEffect, useRef, useState } from 'react';
import { listStore } from 'store/list';

const SearchField = observer(() => {
  const initialMount = useRef(true);
  const [searchFieldValue, setSearchFieldValue] = useState('');
  const debouncedValue = useDebounce<string>(searchFieldValue, 500);

  useEffect(() => {
    listStore.setSearchValue(debouncedValue);
    if (!initialMount.current) {
      listStore.nowPage(1);
    } else {
      initialMount.current = false;
    }
  }, [debouncedValue]);

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSearchFieldValue(event.target.value);
  };

  return (
    <Paper elevation={5}>
      <TextField
        onChange={handleChange}
        fullWidth
        placeholder="Search"
        color="primary"
        variant="outlined"
        value={searchFieldValue}
      />
    </Paper>
  );
});

export default SearchField;
