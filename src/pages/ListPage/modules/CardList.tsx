import { Divider, Grid, useTheme } from '@mui/material';
import { observer } from 'mobx-react-lite';
import React, { useMemo } from 'react';
import { listStore } from 'store/list';
import { Card } from 'ui';
import ListPagination from './ListPagination';
import SearchField from './SearchField';

const CardList = observer(() => {
  // eslint-disable-next-line react-hooks/exhaustive-deps
  const peoples = useMemo(() => listStore.peopleData.results, [listStore.peopleData.results]);
  const theme = useTheme();

  return (
    <>
      <Grid alignItems={'flex-start'} container spacing={3}>
        <Grid item xs={12} sm={6} md={4}>
          <SearchField />
        </Grid>
        <Grid item xs={12} sm={6} md={4}>
          <ListPagination />
        </Grid>
      </Grid>
      <Divider sx={{ mb: 2, background: theme.palette.primary.main }} />
      <Grid container spacing={3}>
        {peoples.map((peopleInfo) => (
          <Grid key={peopleInfo.name || ''} item xs={12} sm={6} md={4} lg={4}>
            <Card peopleInfo={peopleInfo} />
          </Grid>
        ))}
      </Grid>
    </>
  );
});

export default CardList;
