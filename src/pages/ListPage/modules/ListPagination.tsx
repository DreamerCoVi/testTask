import { Pagination, Paper } from '@mui/material';
import { Box } from '@mui/system';
import { observer } from 'mobx-react-lite';
import React from 'react';
import { listStore } from 'store/list';

const ListPagination = observer(() => {
  const handleChange = (event: React.ChangeEvent<unknown>, newValue: number) => {
    listStore.nowPage(newValue);
  };

  return (
    <Box sx={{ justifyContent: 'center', display: 'flex', mb: 3 }}>
      <Paper
        elevation={5}
        style={{
          width: '100%',
          display: 'flex',
          justifyContent: 'center',
          padding: 12.5,
        }}
      >
        <Pagination
          size="medium"
          page={listStore.page}
          siblingCount={0}
          onChange={handleChange}
          count={Math.ceil(listStore.peopleData.count / 10)}
          color="secondary"
        />
      </Paper>
    </Box>
  );
});

export default ListPagination;
