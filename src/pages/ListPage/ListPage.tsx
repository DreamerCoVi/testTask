import { Container } from '@mui/system';
import { observer } from 'mobx-react-lite';
import React, { useEffect } from 'react';
import { listStore } from 'store/list';
import MidscreenLoader from 'ui/MidscreenLoader/MidscreenLoader';
import CardList from './modules/CardList';

const ListPage = observer(() => {
  useEffect(() => {
    const controller = new AbortController();

    listStore.getList(controller);

    return () => {
      controller.abort();
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [listStore.page, listStore.searchValue]);

  return (
    <div>
      <Container maxWidth="lg">
        {listStore.isLoading && <MidscreenLoader />}
        <CardList />
      </Container>
    </div>
  );
});

export default ListPage;
