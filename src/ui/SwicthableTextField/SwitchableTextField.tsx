import { Input, InputProps, Typography } from '@mui/material';
import { Box } from '@mui/system';
import React, { FC, useState } from 'react';
import { capitalize } from 'utils/capitalize';

export interface SwitchableTextFieldProps extends InputProps {
  label?: string;
}

const SwitchableTextField: FC<SwitchableTextFieldProps> = (props) => {
  const [edited, setEdited] = useState(false);

  const handleFocus = (e: React.FocusEvent<HTMLInputElement>) => {
    setEdited(true);
    props.onFocus && props.onFocus(e);
  };
  const handleBlur = (e: React.FocusEvent<HTMLInputElement>) => {
    setEdited(false);
    props.onBlur && props.onBlur(e);
  };

  return (
    <Box
      sx={{
        '& input': {
          textAlign: 'center',
        },
      }}
    >
      {props.label && (
        <Typography align="center" variant="subtitle2" component={'div'}>
          {capitalize(props.label)}
        </Typography>
      )}
      <Box
        style={{
          width: '100%',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
        }}
      >
        <Input {...props} onFocus={handleFocus} onBlur={handleBlur} disableUnderline={!edited} />
      </Box>
    </Box>
  );
};

export default SwitchableTextField;
