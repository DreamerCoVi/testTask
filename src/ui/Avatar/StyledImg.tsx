import { styled } from '@mui/material/styles';
import { ThemeColors } from 'theme';

const StyledImg = styled('img')(() => ({
  borderRadius: '100%',
  maxWidth: 150,
  background: 'white',
  maxHeight: 150,
  boxShadow: '0px -2px 24px 0px rgba(34, 60, 80, 0.5)',
  boxSizing: 'border-box',
  height: 150,
  width: 150,
  border: ThemeColors.mainBrand,
}));

export default StyledImg;
