import React, { FC } from 'react';
import StyledImg from './StyledImg';

interface AvatarProps {
  alt: string;
  query: string;
}

const Avatar: FC<AvatarProps> = ({ alt, query }) => {
  return <StyledImg alt={alt} src={`https://source.unsplash.com/random/${query}/?galaxy`} />;
};

export default Avatar;
