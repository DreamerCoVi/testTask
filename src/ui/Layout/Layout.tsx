import { Box } from '@mui/system';
import { styled } from '@mui/material/styles';
import React from 'react';

const Layout = styled(Box)(({ theme }) => ({
  padding: theme.spacing(4),
  margin: theme.spacing(-2),
  display: 'flex',
  alignItems: 'center',
  flexWrap: 'wrap',

  '& > div': {
    margin: theme.spacing(2),
    width: '100%',
  },
}));

export default Layout;
