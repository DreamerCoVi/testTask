import { CircularProgress } from '@mui/material';
import { Box } from '@mui/system';
import React from 'react';

const MidscreenLoader = () => {
  return (
    <Box
      sx={{
        position: 'absolute',
        width: 'min(90vw, 250px)',
        height: 'min(90vw, 250px)',
        background: 'rgba(0,0,0,.7)',
        borderRadius: 4,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
      }}
    >
      <CircularProgress color="secondary" />
    </Box>
  );
};

export default MidscreenLoader;
