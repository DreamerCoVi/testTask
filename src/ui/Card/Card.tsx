import {
  Box,
  Card as MuiCard,
  CardActionArea,
  CardContent,
  Divider,
  Stack,
  Typography,
} from '@mui/material';
import React, { FC } from 'react';
import { useNavigate } from 'react-router';
import { cardStore } from 'store/card';
import { SinglePeople } from 'types/people';
import { capitalize } from 'utils/capitalize';

interface CardProps {
  peopleInfo: SinglePeople;
}

const fields: Array<keyof SinglePeople> = ['height', 'gender', 'mass'];

const Card: FC<CardProps> = ({ peopleInfo }) => {
  const navigate = useNavigate();

  const handleClick = (e: React.MouseEvent<HTMLElement>) => {
    e.preventDefault();
    cardStore.setCardData(peopleInfo);
    navigate(peopleInfo.url.split('/').at(-2) as string);
  };

  return (
    <MuiCard elevation={5}>
      <CardActionArea onClick={handleClick}>
        <CardContent>
          <Typography variant="h6" component="div">
            {peopleInfo.name}
          </Typography>
          <Typography
            gutterBottom
            variant="body2"
            style={{ opacity: 0.7, marginTop: -4, marginBottom: 16 }}
            component="div"
          >
            {peopleInfo.birth_year}
          </Typography>
          <Stack
            direction="row"
            divider={
              <Divider
                style={{ opacity: 0.5, marginTop: 8, marginBottom: 8 }}
                orientation="vertical"
                flexItem
              />
            }
            flexWrap={'wrap'}
            spacing={3}
            justifyContent="space-evenly"
          >
            {fields.map((field) => (
              <Box key={field}>
                <Typography align="center" variant="subtitle2" component={'div'}>
                  {peopleInfo[field]}
                </Typography>
                <Typography align="center" variant="caption" component={'div'}>
                  {capitalize(field)}
                </Typography>
              </Box>
            ))}
          </Stack>
        </CardContent>
      </CardActionArea>
    </MuiCard>
  );
};

export default Card;
