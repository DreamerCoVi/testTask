import { Typography } from '@mui/material';
import { Box } from '@mui/system';
import React, { FC } from 'react';
import { capitalize } from 'utils/capitalize';

interface InfoBlockProps {
  label: string;
  children: React.ReactNode;
}

const InfoBlock: FC<InfoBlockProps> = ({ label, children }) => {
  return (
    <Box>
      <Typography align="center" variant="subtitle2" component={'div'}>
        {capitalize(label)}
      </Typography>
      <Box
        style={{
          width: '100%',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
        }}
      >
        {children}
      </Box>
    </Box>
  );
};

export default InfoBlock;
