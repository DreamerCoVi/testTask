import { Typography } from '@mui/material';
import React, { FC } from 'react';

interface CardSubtitleProps {
  children: React.ReactNode;
}

const CardSubtitle: FC<CardSubtitleProps> = ({ children }) => {
  return (
    <Typography align="center" style={{ marginTop: 32 }} gutterBottom variant="h6">
      {children}
    </Typography>
  );
};

export default CardSubtitle;
