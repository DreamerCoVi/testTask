import { createTheme } from '@mui/material/styles';

export enum ThemeColors {
  lightShades = '#e2d8d0',
  lightAccent = '#a54a54',
  mainBrand = '#6a5c64',
  darkAccent = '#6f433e',
  shades = '#1f1a1c',
}

export const theme = createTheme({
  palette: {
    primary: {
      main: '#a37c66',
      light: '#d5ab94',
      dark: '#73503b',
    },
    secondary: {
      main: '#9ea093',
      light: '#cfd1c3',
      dark: '#707265',
    },
    error: {
      main: '#f44336',
      light: '#ff7961',
      dark: '#ba000d',
    },
    success: {
      main: '#66a057',
      light: '#96d285',
      dark: '#37712c',
    },
    warning: {
      main: '#e3901f',
      light: '#ffc053',
      dark: '#ac6200',
    },
    background: {
      paper: '#f1f3ef',
    },
    text: {
      primary: '#030303',
      secondary: '#e2d8d0',
      disabled: '#8e8e8e',
    },
    info: {
      main: '#35203c',
      light: '#5f4867',
      dark: '#110017',
    },
  },
});
