import { PeopleResponse } from 'types/people';
import { ListStore } from './list.store';

interface IListStore {
  peopleData: PeopleResponse;
  searchValue: string;
  isInitialLoading: boolean;
  isLoading: boolean;
  page: number;
  getList: (controller: AbortController) => void;
  nowPage: (value: number) => void;
  setSearchValue: (value: string) => void;
}

export const listStore: IListStore = new ListStore();
