import { getPeopleList } from 'api/list';
import { makeAutoObservable } from 'mobx';
import { ResponseCodes } from 'types/request';
import { getId } from 'utils/getId';
import { getFromLocalStorage } from 'utils/localstorage';

export class ListStore {
  peopleData = initialObject;
  searchValue = '';
  isInitialLoading = true;
  isLoading = true;
  page = 1;

  constructor() {
    makeAutoObservable(this);
  }

  getList = async (controller) => {
    this.isLoading = true;
    const res = await getPeopleList({ page: this.page, search: this.searchValue }, controller);
    if (res && res?.code !== ResponseCodes.Cancelled) {
      const currentData = res ? res : initialObject;
      const currentList = currentData.results.map((item) => {
        const cachedCard = getFromLocalStorage(getId(item.url));
        if (cachedCard) return cachedCard;
        return item;
      });
      currentData.results = currentList;
      this.peopleData = currentData;
      this.isLoading = false;
      this.isInitialLoading = false;
    }
  };

  nowPage = (value) => {
    this.page = value;
  };

  setSearchValue = (value) => {
    this.searchValue = value;
  };
}

const initialObject = {
  count: 0,
  next: null,
  previous: null,
  results: [],
};
