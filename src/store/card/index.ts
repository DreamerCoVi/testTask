import { SinglePeople } from 'types/people';
import { CardStore } from './card.store';

interface ICardStore {
  cardData: SinglePeople;
  initialData: SinglePeople;
  isLoading: boolean;
  hasChanges: boolean;
  saveCard: () => void;
  resetCard: () => void;
  changeCardData: (key: keyof SinglePeople, value: string) => void;
  getPeopleCard: (id: number | string) => void;
  setCardData: (value: any) => void;
  clearCardData: () => void;
}

export const cardStore: ICardStore = new CardStore();
