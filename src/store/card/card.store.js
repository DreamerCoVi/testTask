import { getCard } from 'api/card';
import dayjs from 'dayjs';
import { makeAutoObservable } from 'mobx';
import { getFromLocalStorage, setToLocalStorage } from 'utils/localstorage';

export class CardStore {
  cardData = { ...initialObject };
  initialData = { ...initialObject };
  isLoading = false;
  hasChanges = false;

  constructor() {
    makeAutoObservable(this);
  }

  saveCard = () => {
    const newData = { ...this.cardData, edited: dayjs().format('YYYY-MM-DDTHH:mm:ss.SSSSSS[Z]') };
    this.cardData = newData;
    this.initialData = newData;
    setToLocalStorage(newData.url.split('/').at(-2), newData);
  };

  resetCard = () => {
    this.cardData = { ...this.initialData };
  };

  clearCardData = () => {
    this.cardData = initialObject;
  };

  changeCardData = (key, value) => {
    this.cardData[key] = value;
  };

  getPeopleCard = async (id) => {
    this.isLoading = true;
    const savedPeopleCard = getFromLocalStorage(id);
    if (savedPeopleCard) {
      this.cardData = savedPeopleCard;
      this.initialData = savedPeopleCard;
      this.isLoading = false;
      return;
    }

    const res = await getCard({ id });
    if (res) {
      this.cardData = res;
      this.initialData = res;
      this.isLoading = false;
    }
  };

  setCardData = (value) => {
    this.cardData = value;
    this.initialData = value;
  };
}

const initialObject = {
  birth_year: '',
  created: '',
  edited: '',
  eye_color: '',
  films: [],
  gender: '',
  hair_color: '',
  height: '',
  homeworld: '',
  mass: '',
  name: '',
  skin_color: '',
  species: [],
  starships: [],
  url: '',
  vehicles: [],
};
