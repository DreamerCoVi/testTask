import { instance } from './instance';

type Params = {
  id: number;
};

type GetCard = (params: Params) => Promise<any>;

export const getCard: GetCard = async (params) => {
  try {
    const { data } = await instance.get(`/people/${params.id}`);
    return data;
  } catch (e) {
    return e;
  }
};
