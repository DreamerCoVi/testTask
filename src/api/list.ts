import { instance } from './instance';

type Params = {
  page: number;
};

type GetPeopleList = (params: Params, controller: AbortController) => Promise<any>;

export const getPeopleList: GetPeopleList = async (params, controller) => {
  try {
    const { data } = await instance.get('/people', {
      params,
      signal: controller.signal,
    });
    return data;
  } catch (e) {
    return e;
  }
};
